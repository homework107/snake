const ROWS = 10
const COLLUMS = 10
const CELL_SIZE = 70
const CELL_MARGIN = 3
const GAME_PUDDING = 10
const START_COLDOWN = 400
const FOOD_COLOR = 'orange'
const BAD_FOOD_COLOR = 'red'
const SNAKE_COLOR = 'grey'
const FREE_COLOR = 'white'
const LEVEL_COOLDOWN = 20

const canvas = document.getElementById('canvas')
const context = canvas.getContext('2d')

canvas.width = COLLUMS * CELL_SIZE + (COLLUMS - 1) * CELL_MARGIN + 2 * GAME_PUDDING
canvas.height = ROWS * CELL_SIZE + (ROWS - 1) * CELL_MARGIN + 2 * GAME_PUDDING

let map = createGameMap(COLLUMS, ROWS)
let play = true
let cooldown = START_COLDOWN
let prevTick = 0

getRandomFreeCell(map).food = true
getRandomFreeCell(map).poison = true

const cell = getRandomFreeCell(map)
let snake = [cell]

cell.snake = true

let snakeDirect = 'up'
let nextSnakeDirect = 'up'

requestAnimationFrame(loop)

function loop(timestamp){
    requestAnimationFrame(loop)
    clearCanvas()
    if(prevTick + cooldown <= timestamp && play){
        prevTick = timestamp
        snakeDirect = nextSnakeDirect
        moveSnake()
        const head = snake[0]
        const tail = snake[snake.length - 1]

        if(head.food){
            head.food = false
            snake.push(tail)
            getRandomFreeCell(map).food = true
            cooldown -= LEVEL_COOLDOWN 
        }

        else{
            let isEnd = false
            if(head.poison){
                head.poison = false
                snake.pop()
                if(snake.length !==0){
                    getRandomFreeCell(map).poison = true
                }
                else{
                    isEnd = true
                }
            }
            
            for(let i = 1; i < snake.length; i++){
                if(snake[i] === snake[0]){
                    isEnd = true
                    break
                }
            }
    
            if(isEnd == true){
                // alert('конец игры')
                play = false
            }
        }

    }
    drowGameMap(map)
    showState()
    if(!play){
        drawPaused()
    }
}

document.addEventListener('keydown', function (event) {
    if (event.key === "ArrowUp") {
        if (snake.length === 1 || snakeDirect === "left" || snakeDirect === "right") {
            nextSnakeDirect = 'up'
        }
    }

    else if (event.key === "ArrowDown") {
        if (snake.length === 1 || snakeDirect === "left" || snakeDirect === "right") {
            nextSnakeDirect = 'down'
        }
    }

    else if (event.key === "ArrowLeft") {
        if (snake.length === 1 || snakeDirect === "up" || snakeDirect === "down") {
            nextSnakeDirect = 'left'
        }
    }

    else if (event.key === "ArrowRight") {
        if (snake.length === 1 || snakeDirect === "up" || snakeDirect === "down") {
            nextSnakeDirect = 'right'
        }
    }

    else if (event.key === "Enter") {
        if (play) {
            return            
        }

        init()
    }
})

// drowGameMap(map)
// showState()
